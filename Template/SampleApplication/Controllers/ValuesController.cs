﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace SampleApplication.Controllers
{
    [ApiController]
    [Route("")]
    public class ValuesController : ControllerBase
    {
        [Authorize(Policy = "TeacherOnly") ,HttpGet("/teachers")]
        public ActionResult<IEnumerable<string>> Teachers()
        {
            return Ok(new[] {"Dr Who", "Dr How", "Jerry Zhang"});
        }


        [Authorize(Policy = "StudentOnly") ,HttpGet("/students")]
        public ActionResult<IEnumerable<string>> Students()
        {
            return Ok(new[] {"Alice", "Bob", "Eve"});
        }

        [Authorize, HttpGet("/events")]
        public ActionResult<IEnumerable<string>> SchoolEvents()
        {
            return Ok(new[] {"Union Day", "Movie Day", "Graduation Day"});
        }
        
        [HttpGet("/all")]
        public ActionResult<IEnumerable<string>> Anime()
        {
            return Ok(new[] {"Madoka Magica", "Bunny Girl Senpai", "Humanity has Declined"});
        }
        
        
    }
}