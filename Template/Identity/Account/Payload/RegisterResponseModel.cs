namespace Identity.Account.Payload
{
    public class RegisterResponseModel
    {
        public string Id { get; set; }
        public string Email { get; set; }
    }
}