namespace Identity.Account.Payload
{
    public class LoginResponseModel
    {
        public string Id { get; set; }
        public string Token { get; set; }
    }
}